// code-examples/IntroducingScala/upper1-script.scala
class Upper {
	def upper(strings: String*): Seq[String] = {
		strings.map((s:String) => s.toUpperCase())
	}
}
object Lower {
	def lower(strings: String*): Seq[String] = {
		strings.map((s:String) => s.toLowerCase())
	}
}
val up = new Upper
Console.println(up.upper("A", "First", "Scala", "Program"))
Console.println(Lower.lower("A","B","c","0"))